import { AuditRecord, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
export declare type AuditInit = AuditRecord;
export declare type searchIdentitiesByNameOutputRecord = {
    status: number;
    recordsNumber: number;
    transactionId: 0;
    identities: Identity[];
    callSequenceId: string;
    callSequenceDate: string;
};
export declare type Identity = {
    birthDate: string;
    birthPlace: string;
    cancelReason: {
        id: string;
        description: string;
        cancelDate: string;
    };
    fatherName: string;
    fatherNameLatin: string;
    gender: "Α" | "Γ";
    idnumber: string;
    isActive: boolean | "true" | "True" | "false" | "False";
    issueDate: string;
    issueInstitution: {
        id: string;
        description: string;
    };
    lastUpdateDate: string;
    motherName: string;
    municipality: string;
    name: string;
    nameLatin: string;
    populationRegistryNo: string;
    previousId: string | null;
    previousIdNumber: string;
    surname: string;
    surnameLatin: string;
};
export declare type errorRecord = {
    errorCode: string;
    errorDescr: string;
};
export declare type searchIdentitiesByNameInputRecord = {
    name: string;
    surname: string;
    fatherName: string;
    motherName: string;
    gender: "Α" | "Γ" | string;
    birthYearFrom: string;
    birthYearTo: string;
    reasonDesc: string;
};
/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export declare type overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
/**
 *
 * @param input searchIdentitiesByNameInputRecord;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns searchIdentitiesByNameOutputRecord | errorRecord
 */
export declare const getIdentitys: (input: searchIdentitiesByNameInputRecord, user: string, pass: string, overrides?: overrides | undefined) => Promise<any>;
export default getIdentitys;
