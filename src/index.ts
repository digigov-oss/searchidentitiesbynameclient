import soapClient from './soapClient.js';
import {generateAuditRecord, AuditRecord, FileEngine, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
import config from './config.json'; 


export type AuditInit = AuditRecord;
export type searchIdentitiesByNameOutputRecord = {
    status:number;
    recordsNumber:number; 
    transactionId:0; 
    identities:Identity[]
    callSequenceId: string; 
    callSequenceDate:string; 
}

export type Identity = {
        birthDate:string;
  birthPlace:string; 
  cancelReason: {
           id:string;
           description:string;
           cancelDate:string;
          }
  fatherName:string; 
  fatherNameLatin:string;
       gender:"Α"|"Γ"; 
       idnumber:string;
       isActive:boolean|"true"|"True"|"false"|"False";
       issueDate:string;
       issueInstitution:{ 
                id:string;
                description:string;
              } 
       lastUpdateDate:string;
       motherName:string;
       municipality:string;
       name:string;
       nameLatin:string;
       populationRegistryNo:string;
       previousId:string|null;
       previousIdNumber:string;
       surname:string;
       surnameLatin:string;
     }

export type errorRecord = {
    errorCode:string;
    errorDescr:string;
}

export type searchIdentitiesByNameInputRecord = {
    name: string;
    surname: string;
    fatherName: string;
    motherName : string;
    gender: "Α"|"Γ"|string; 
    birthYearFrom:string; 
    birthYearTo:string;
    reasonDesc:string;
    }

/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export type overrides = {
   endpoint?:string;
   prod?:boolean;
   auditInit?: AuditRecord;
   auditStoragePath?: string;
   auditEngine?: AuditEngine;
}

/**
 * 
 * @param input searchIdentitiesByNameInputRecord;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns searchIdentitiesByNameOutputRecord | errorRecord
 */
export const getIdentitys = async (input:searchIdentitiesByNameInputRecord, user:string, pass:string, overrides?:overrides) => {
    const endpoint = overrides?.endpoint ?? "";
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? {} as AuditRecord;
    const auditStoragePath = overrides?.auditStoragePath ?? "/tmp"
    const auditEngine = overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod==true? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord) throw new Error('Audit record is not initialized');
  
    try {
        const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
        const Identitys = await s.getIdentitys(input);
        return {...Identitys,...auditRecord};
       } catch (error) {
           throw(error);
       }
}
export default getIdentitys;